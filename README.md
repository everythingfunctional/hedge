# Hedge

[![pipeline status](https://gitlab.com/everythingfunctional/hedge/badges/master/pipeline.svg)](https://gitlab.com/everythingfunctional/hedge/commits/master)

Hedge your tests.

The Hedge test framework is quite similar to the Hspec framework, with a major
difference. It is possible for Hedge to report your **passing** test results
as well. This can serve as a sort of V&V report for your code.

This idea was taken for a testing framework I wrote for Fortran.
[Vegetables](https://gitlab.com/everythingfunctional/vegetables)

Many of the design principles for Vegetables were taken from the Hspec
testing framework, then the additional functionality I included seemed like it
would be quite useful in a Haskell project as well.
