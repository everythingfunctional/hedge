{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE UndecidableInstances #-}
module Hedge where

import           Control.Monad                  ( unless
                                                , when
                                                )
import           Data.List                      ( genericLength
                                                , intercalate
                                                , isInfixOf
                                                )
import           Data.Maybe                     ( mapMaybe )
import           Data.String.Utils              ( rstrip )
import           Options.Applicative            ( Parser
                                                , (<**>)
                                                , auto
                                                , execParser
                                                , fullDesc
                                                , header
                                                , help
                                                , helper
                                                , info
                                                , long
                                                , metavar
                                                , option
                                                , optional
                                                , progDesc
                                                , short
                                                , showDefault
                                                , strOption
                                                , switch
                                                , value
                                                )
import           Prelude                 hiding ( and )
import           System.Exit                    ( exitFailure
                                                , exitSuccess
                                                )
import qualified Test.QuickCheck.Counterexamples
                                               as QC
                                                ( Result(Failure, Success) )
import           Test.QuickCheck.Counterexamples
                                                ( Arbitrary
                                                , Args
                                                , (:&:)((:&:))
                                                , arbitrary
                                                , chatty
                                                , maxSuccess
                                                , output
                                                , quickCheckWithResult
                                                , stdArgs
                                                )
import           Text.Regex.PCRE                ( (=~) )

class AltShow a where
  altShow :: a -> String

instance {-# OVERLAPPABLE #-} Show a => AltShow a where
    altShow = show

instance {-# OVERLAPS #-} AltShow String where
    altShow string = "\"" ++ string ++ "\""

data Arguments = Arguments
    { quiet :: Bool
    , verbose :: Bool
    , colorOff :: Bool
    , filterString :: Maybe String
    , numRandom :: Int }

indentation = 4

data IndividualResult = IndividualResult { message :: String, succeeded :: Bool }

newtype Result = Result [IndividualResult]

data Test a where
    TestCase :: String -> Result -> Test ()
    InputTestCase :: String -> (a -> Result) -> Test a
    TestCaseWithExamples :: forall b. String -> [b] -> (b -> Result) -> Test ()
    TestCaseWithGenerator :: (Arbitrary a, Show a) => String -> (a -> Result) -> Test ()
    TestCollection :: String -> [Test ()] -> Test ()
    TestCollectionWithInput :: forall b. String -> b -> [Test b] -> Test ()
    TransformingTestCollection :: forall b c. String -> (b -> c) -> [Test c] -> Test b

data TestResult =
      TestCaseResult String Result
    | TestCollectionResult String [TestResult]

description :: Test a -> String
description (TestCase      description' _         ) = description'
description (InputTestCase description' _         ) = description'
description (TestCaseWithExamples description' _ _) = description'
description (TestCaseWithGenerator description' _ ) = description'
description (TestCollection description' tests) =
    makeCollectionDescription description' tests
description (TestCollectionWithInput description' _ tests) =
    makeCollectionDescription description' tests
description (TransformingTestCollection description' _ tests) =
    makeCollectionDescription description' tests

makeCollectionDescription :: String -> [Test a] -> String
makeCollectionDescription collectionDescription tests =
    let testDescriptions = map description tests
    in  hangingIndent (unlines (collectionDescription : testDescriptions))
                      indentation

hangingIndent :: String -> Int -> String
hangingIndent string spaces =
    intercalate ("\n" ++ replicate spaces ' ') (lines string)

indent :: String -> Int -> String
indent string spaces = unlines (map (replicate spaces ' ' ++) (lines string))

delimit :: String -> String
delimit string = "|" ++ string ++ "|"

empty :: [a] -> Bool
empty [] = True
empty _  = False

notEmpty :: [a] -> Bool
notEmpty = not . empty

count :: Integral b => Test a -> b
count TestCase{}                             = 1
count InputTestCase{}                        = 1
count TestCaseWithExamples{}                 = 1
count TestCaseWithGenerator{}                = 1
count (TestCollection _ tests              ) = sum (map count tests)
count (TestCollectionWithInput    _ _ tests) = sum (map count tests)
count (TransformingTestCollection _ _ tests) = sum (map count tests)

run :: Args -> Test () -> IO TestResult
run _ (TestCase description' result) =
    return $ TestCaseResult description' result
run _ (InputTestCase description' _) = return $ TestCaseResult
    description'
    (fail' "How on earth did you manage to not provide an input?")
run _ (TestCaseWithExamples description' examples test) =
    let results = foldl and emptyResult (map test examples)
    in  return $ TestCaseResult description' results
run qcArgs (TestCaseWithGenerator description' test) = do
    qcResults <- quickCheckWithResult qcArgs (passed' . test)
    let result' = case qcResults of
            (Nothing, QC.Success { output = output }) ->
                succeed (rstrip output)
            (Just (x :&: ()), QC.Failure { output = output }) ->
                fail' (rstrip output) `and` test x
    return $ TestCaseResult description' result'
run qcArgs (TestCollection description' tests) = do
    results <- mapM (run qcArgs) tests
    return $ TestCollectionResult description' results
run qcArgs (TestCollectionWithInput description' input tests) = do
    results <- mapM (runWithInput qcArgs input) tests
    return $ TestCollectionResult description' results
run _ (TransformingTestCollection description' _ _) = return $ TestCaseResult
    description'
    (fail' "How on earth did you manage to not provide an input?")

runWithInput :: Args -> a -> Test a -> IO TestResult
runWithInput qcArgs _ test@TestCase{} = run qcArgs test
runWithInput _ input (InputTestCase description' test) =
    return $ TestCaseResult description' (test input)
runWithInput qcArgs _ test@TestCaseWithExamples{}    = run qcArgs test
runWithInput qcArgs _ test@TestCaseWithGenerator{}   = run qcArgs test
runWithInput qcArgs _ test@TestCollection{}          = run qcArgs test
runWithInput qcArgs _ test@TestCollectionWithInput{} = run qcArgs test
runWithInput qcArgs input (TransformingTestCollection description' transformer tests)
    = do
        results <- mapM (runWithInput qcArgs (transformer input)) tests
        return $ TestCollectionResult description' results

filter' :: String -> Test a -> Maybe (Test a)
filter' regex test@(TestCase description' _) =
    if description' =~ regex then Just test else Nothing
filter' regex test@(InputTestCase description' _) =
    if description' =~ regex then Just test else Nothing
filter' regex test@(TestCaseWithExamples description' _ _) =
    if description' =~ regex then Just test else Nothing
filter' regex test@(TestCaseWithGenerator description' _) =
    if description' =~ regex then Just test else Nothing
filter' regex test@(TestCollection description' tests) =
    if description' =~ regex
        then Just test
        else case mapMaybe (filter' regex) tests of
            []       -> Nothing
            filtered -> Just (TestCollection description' filtered)
filter' regex test@(TestCollectionWithInput description' input tests) =
    if description' =~ regex
        then Just test
        else case mapMaybe (filter' regex) tests of
            [] -> Nothing
            filtered ->
                Just (TestCollectionWithInput description' input filtered)
filter' regex test@(TransformingTestCollection description' transformer tests)
    = if description' =~ regex
        then Just test
        else case mapMaybe (filter' regex) tests of
            [] -> Nothing
            filtered ->
                Just
                    (TransformingTestCollection description'
                                                transformer
                                                filtered
                    )

runTest :: IO (Test ()) -> IO ()
runTest test' = do
    test <- test'
    args <- getArguments
    let colorize = not (colorOff args)
    putStrLn "\nRunning Tests\n"
    let maybeTest = case filterString args of
            Nothing            -> Just test
            Just filterString' -> filter' filterString' test
    case maybeTest of
        Nothing        -> putStrLn "No matching tests found"
        Just testToRun -> do
            unless (quiet args) $ putStrLn $ description testToRun
            putStrLn
                $  "\nA total of "
                ++ show (count testToRun)
                ++ " test cases\n"
            result <- run
                (stdArgs { chatty = False, maxSuccess = numRandom args })
                testToRun
            if passed result
                then do
                    putStrLn "\nAll Passed\n"
                    when (verbose args) $ putStrLn $ verboseDescription
                        colorize
                        result
                    putStrLn
                        $  "\nA total of "
                        ++ show (countAllCases result)
                        ++ " test cases containing a total of "
                        ++ show (countAllAsserts result)
                        ++ " asserts"
                    exitSuccess
                else do
                    putStrLn "\nFailed\n"
                    if verbose args
                        then putStrLn $ verboseDescription colorize result
                        else putStrLn $ failureDescription colorize result
                    putStrLn
                        $  "\n"
                        ++ show (countFailingCases result)
                        ++ " of "
                        ++ show (countAllCases result)
                        ++ " cases failed"
                    putStrLn
                        $  show (countFailingAsserts result)
                        ++ " of "
                        ++ show (countAllAsserts result)
                        ++ " asserts failed"
                    exitFailure

getArguments :: IO Arguments
getArguments = execParser
    (info
        (argumentParser <**> helper)
        (fullDesc <> progDesc "Run the test suite" <> header
            "hedge - a testing framework"
        )
    )

argumentParser :: Parser Arguments
argumentParser =
    Arguments
        <$> switch
                (long "quiet" <> short 'q' <> help
                    "Don't print the initial test description"
                )
        <*> switch
                (long "verbose" <> short 'v' <> help "Show all the test results"
                )
        <*> switch
                (long "collor-off" <> short 'c' <> help
                    "Don't colorize the output"
                )
        <*> optional
                (strOption
                    (long "filter" <> short 'f' <> metavar "REGEX" <> help
                        "Only run the tests that match the given REGEX"
                    )
                )
        <*> option
                auto
                (  long "numrand"
                <> short 'n'
                <> help
                       "Number of random values to use for each test with generated values"
                <> showDefault
                <> value 100
                <> metavar "NUM"
                )

describe :: String -> [Test ()] -> Test ()
describe = TestCollection

it :: String -> Result -> Test ()
it = TestCase

itAlways :: (Arbitrary a, Show a) => String -> (a -> Result) -> Test ()
itAlways = TestCaseWithGenerator

given :: String -> [Test ()] -> Test ()
given description' = TestCollection ("Given " ++ description')

givenInput :: String -> a -> [Test a] -> Test ()
givenInput description' = TestCollectionWithInput ("Given " ++ description')

whenTransformed :: String -> (a -> b) -> [Test b] -> Test a
whenTransformed description' =
    TransformingTestCollection ("When " ++ description')

whenWithInput :: String -> a -> [Test a] -> Test ()
whenWithInput description' = TestCollectionWithInput ("When " ++ description')

then' :: String -> (a -> Result) -> Test a
then' description' = InputTestCase ("Then " ++ description')

testThat :: [IO (Test ())] -> IO (Test ())
testThat tests = do
    tests' <- sequence tests
    return $ TestCollection "Test that" tests'

succeed :: String -> Result
succeed message = Result [IndividualResult message True]

fail' :: String -> Result
fail' message = Result [IndividualResult message False]

and :: Result -> Result -> Result
(Result first) `and` (Result second) = Result (first ++ second)

emptyResult :: Result
emptyResult = Result []

passed :: TestResult -> Bool
passed (TestCaseResult       _ result ) = passed' result
passed (TestCollectionResult _ results) = all passed results

passed' :: Result -> Bool
passed' (Result results) = all succeeded results

countAllCases :: Integral a => TestResult -> a
countAllCases (TestCaseResult _ result) = 1
countAllCases (TestCollectionResult _ results) =
    sum (map countAllCases results)

countFailingCases :: Integral a => TestResult -> a
countFailingCases (TestCaseResult _ result) = if passed' result then 0 else 1
countFailingCases (TestCollectionResult _ results) =
    sum (map countFailingCases results)

countAllAsserts :: Integral a => TestResult -> a
countAllAsserts (TestCaseResult _ result) = countAllAsserts' result
countAllAsserts (TestCollectionResult _ results) =
    sum (map countAllAsserts results)

countAllAsserts' :: Integral a => Result -> a
countAllAsserts' (Result asserts) = genericLength asserts

countFailingAsserts :: Integral a => TestResult -> a
countFailingAsserts (TestCaseResult _ result) = countFailingAsserts' result
countFailingAsserts (TestCollectionResult _ results) =
    sum (map countFailingAsserts results)

countFailingAsserts' :: Integral a => Result -> a
countFailingAsserts' (Result asserts) =
    genericLength (filter (not . succeeded) asserts)

verboseDescription :: Bool -> TestResult -> String
verboseDescription colorize (TestCaseResult description' result) =
    hangingIndent
        (unlines (description' : verboseDescription' colorize result))
        indentation
verboseDescription colorize (TestCollectionResult description' results) =
    let resultDescriptions = map (verboseDescription colorize) results
    in  hangingIndent (unlines (description' : resultDescriptions)) indentation

verboseDescription' :: Bool -> Result -> [String]
verboseDescription' colorize (Result results) =
    map (verboseDescription'' colorize) results

verboseDescription'' :: Bool -> IndividualResult -> String
verboseDescription'' colorize result = if colorize
    then if succeeded result
        then "\ESC[32m" ++ message result ++ "\ESC[0m"
        else "\ESC[31m" ++ message result ++ "\ESC[0m"
    else message result

failureDescription :: Bool -> TestResult -> String
failureDescription colorize testResult@(TestCaseResult description' result) =
    if passed testResult
        then ""
        else hangingIndent
            (unlines (description' : failureDescription' colorize result))
            indentation
failureDescription colorize testResult@(TestCollectionResult description' results)
    = if passed testResult
        then ""
        else
            let resultDescriptions =
                    filter notEmpty $ map (failureDescription colorize) results
            in  hangingIndent (unlines (description' : resultDescriptions))
                              indentation

failureDescription' :: Bool -> Result -> [String]
failureDescription' colorize (Result results) =
    map (failureDescription'' colorize) (filter (not . succeeded) results)

failureDescription'' :: Bool -> IndividualResult -> String
failureDescription'' colorize result = if colorize
    then "\ESC[31m" ++ message result ++ "\ESC[0m"
    else message result

assertAll :: (a -> Result) -> [a] -> Result
assertAll assertion items = foldr (and . assertion) emptyResult items

assertDoesntInclude :: (Eq a, AltShow [a]) => [a] -> [a] -> Result
assertDoesntInclude subSequence list =
    assertDoesntIncludeWithMessage subSequence list ""

assertDoesntIncludeWithMessage
    :: (Eq a, AltShow [a]) => [a] -> [a] -> String -> Result
assertDoesntIncludeWithMessage subSequence list message =
    assertDoesntIncludeWithMessages subSequence list message message

assertDoesntIncludeWithMessages
    :: (Eq a, AltShow [a]) => [a] -> [a] -> String -> String -> Result
assertDoesntIncludeWithMessages subSequence list successMessage failureMessage
    = if subSequence `isInfixOf` list
        then fail'
            (withUserMessage
                (makeDoesntIncludeFailureMessage (altShow subSequence)
                                                 (altShow list)
                )
                failureMessage
            )
        else succeed
            (withUserMessage
                (makeDoesntIncludeSuccessMessage (altShow subSequence)
                                                 (altShow list)
                )
                successMessage
            )

assertEmpty :: AltShow [a] => [a] -> Result
assertEmpty list = assertEmptyWithMessage list ""

assertEmptyWithMessage :: AltShow [a] => [a] -> String -> Result
assertEmptyWithMessage list message =
    assertEmptyWithMessages list message message

assertEmptyWithMessages :: AltShow [a] => [a] -> String -> String -> Result
assertEmptyWithMessages list successMessage failureMessage = if empty list
    then succeed (withUserMessage "List was empty" successMessage)
    else
        fail'
            (withUserMessage (makeEmptyFailureMessage (altShow list))
                             failureMessage
            )

assertEquals :: (Eq a, AltShow a) => a -> a -> Result
assertEquals expected actual = assertEqualsWithMessage expected actual ""

assertEqualsWithMessage :: (Eq a, AltShow a) => a -> a -> String -> Result
assertEqualsWithMessage expected actual message =
    assertEqualsWithMessages expected actual message message

assertEqualsWithMessages
    :: (Eq a, AltShow a) => a -> a -> String -> String -> Result
assertEqualsWithMessages expected actual successMessage failureMessage =
    if expected == actual
        then succeed
            (withUserMessage (makeEqualsSuccessMessage (altShow expected))
                             successMessage
            )
        else fail'
            (withUserMessage
                (makeEqualsFailureMessage (altShow expected) (altShow actual))
                failureMessage
            )

assertEqualsWithinAbsolute :: (Ord a, Num a, Show a) => a -> a -> a -> Result
assertEqualsWithinAbsolute expected actual tolerance =
    assertEqualsWithinAbsoluteWithMessage expected actual tolerance ""

assertEqualsWithinAbsoluteWithMessage
    :: (Ord a, Num a, Show a) => a -> a -> a -> String -> Result
assertEqualsWithinAbsoluteWithMessage expected actual tolerance message =
    assertEqualsWithinAbsoluteWithMessages expected
                                           actual
                                           tolerance
                                           message
                                           message

assertEqualsWithinAbsoluteWithMessages
    :: (Ord a, Num a, Show a) => a -> a -> a -> String -> String -> Result
assertEqualsWithinAbsoluteWithMessages expected actual tolerance successMessage failureMessage
    = if equalsWithinAbsolute expected actual tolerance
        then succeed
            (withUserMessage
                (makeWithinSuccessMessage (show expected)
                                          (show actual)
                                          (show tolerance)
                )
                successMessage
            )
        else fail'
            (withUserMessage
                (makeWithinFailureMessage (show expected)
                                          (show actual)
                                          (show tolerance)
                )
                failureMessage
            )

assertEqualsWithinRelative
    :: (Ord a, Fractional a, Show a) => a -> a -> a -> Result
assertEqualsWithinRelative expected actual tolerance =
    assertEqualsWithinRelativeWithMessage expected actual tolerance ""

assertEqualsWithinRelativeWithMessage
    :: (Ord a, Fractional a, Show a) => a -> a -> a -> String -> Result
assertEqualsWithinRelativeWithMessage expected actual tolerance message =
    assertEqualsWithinRelativeWithMessages expected
                                           actual
                                           tolerance
                                           message
                                           message

assertEqualsWithinRelativeWithMessages
    :: (Ord a, Fractional a, Show a)
    => a
    -> a
    -> a
    -> String
    -> String
    -> Result
assertEqualsWithinRelativeWithMessages expected actual tolerance successMessage failureMessage
    = if equalsWithinRelative expected actual tolerance
        then succeed
            (withUserMessage
                (makeWithinSuccessMessage (show expected)
                                          (show actual)
                                          (show (tolerance / 100) ++ "%")
                )
                successMessage
            )
        else fail'
            (withUserMessage
                (makeWithinFailureMessage (show expected)
                                          (show actual)
                                          (show (tolerance / 100) ++ "%")
                )
                failureMessage
            )

assertIncludes :: (Eq a, AltShow [a]) => [a] -> [a] -> Result
assertIncludes searchFor string = assertIncludesWithMessage searchFor string ""

assertIncludesWithMessage
    :: (Eq a, AltShow [a]) => [a] -> [a] -> String -> Result
assertIncludesWithMessage searchFor string message =
    assertIncludesWithMessages searchFor string message message

assertIncludesWithMessages
    :: (Eq a, AltShow [a]) => [a] -> [a] -> String -> String -> Result
assertIncludesWithMessages subSequence list successMessage failureMessage =
    if subSequence `isInfixOf` list
        then succeed
            (withUserMessage
                (makeIncludesSuccessMessage (altShow subSequence) (altShow list)
                )
                successMessage
            )
        else fail'
            (withUserMessage
                (makeIncludesFailureMessage (altShow subSequence) (altShow list)
                )
                failureMessage
            )

assertNot :: Bool -> Result
assertNot condition = assertNotWithMessage condition ""

assertNotWithMessage :: Bool -> String -> Result
assertNotWithMessage condition message =
    assertNotWithMessages condition message message

assertNotWithMessages :: Bool -> String -> String -> Result
assertNotWithMessages condition successMessage failureMessage = if condition
    then fail' (withUserMessage "Expected to not be true" failureMessage)
    else succeed (withUserMessage "Was not true" successMessage)

assertThat :: Bool -> Result
assertThat condition = assertThatWithMessage condition ""

assertThatWithMessage :: Bool -> String -> Result
assertThatWithMessage condition message =
    assertThatWithMessages condition message message

assertThatWithMessages :: Bool -> String -> String -> Result
assertThatWithMessages condition successMessage failureMessage = if condition
    then succeed (withUserMessage "Was true" successMessage)
    else fail' (withUserMessage "Expected to be true" failureMessage)

makeDoesntIncludeFailureMessage :: String -> String -> String
makeDoesntIncludeFailureMessage searchFor string = hangingIndent
    (  "Expected\n"
    ++ indent (delimit (hangingIndent string 1)) indentation
    ++ "to not include\n"
    ++ indent (delimit (hangingIndent searchFor 1)) indentation
    )
    indentation

makeDoesntIncludeSuccessMessage :: String -> String -> String
makeDoesntIncludeSuccessMessage searchFor string = hangingIndent
    (  "The list\n"
    ++ indent (delimit (hangingIndent string 1)) indentation
    ++ "didn't include\n"
    ++ indent (delimit (hangingIndent searchFor 1)) indentation
    )
    indentation

makeEmptyFailureMessage :: String -> String
makeEmptyFailureMessage string = hangingIndent
    (  "The list\n"
    ++ indent (delimit (hangingIndent string 1)) indentation
    ++ "wasn't empty"
    )
    indentation

makeEqualsFailureMessage :: String -> String -> String
makeEqualsFailureMessage expected actual = hangingIndent
    (  "Expected\n"
    ++ indent (delimit (hangingIndent expected 1)) indentation
    ++ "but got\n"
    ++ indent (delimit (hangingIndent actual 1)) indentation
    )
    indentation

makeEqualsSuccessMessage :: String -> String
makeEqualsSuccessMessage expected = hangingIndent
    (  "Expected and got\n"
    ++ indent (delimit (hangingIndent expected 1)) indentation
    )
    indentation

makeIncludesFailureMessage :: String -> String -> String
makeIncludesFailureMessage searchFor string = hangingIndent
    (  "Expected\n"
    ++ indent (delimit (hangingIndent string 1)) indentation
    ++ "to include\n"
    ++ indent (delimit (hangingIndent searchFor 1)) indentation
    )
    indentation

makeIncludesSuccessMessage :: String -> String -> String
makeIncludesSuccessMessage searchFor string = hangingIndent
    (  "The list\n"
    ++ indent (delimit (hangingIndent string 1)) indentation
    ++ "included\n"
    ++ indent (delimit (hangingIndent searchFor 1)) indentation
    )
    indentation

makeWithinFailureMessage :: String -> String -> String -> String
makeWithinFailureMessage expected actual tolerance =
    "Expected "
        ++ delimit actual
        ++ " to be within "
        ++ delimit ("±" ++ tolerance)
        ++ " of "
        ++ delimit expected

makeWithinSuccessMessage :: String -> String -> String -> String
makeWithinSuccessMessage expected actual tolerance =
    delimit actual
        ++ " was within "
        ++ delimit ("±" ++ tolerance)
        ++ " of "
        ++ delimit expected

withUserMessage :: String -> String -> String
withUserMessage message [] = message
withUserMessage message userMessage =
    rstrip
        $  message
        ++ "\n"
        ++ indent
               (hangingIndent
                   ("User message:\n" ++ delimit (hangingIndent userMessage 1))
                   indentation
               )
               indentation

equalsWithinAbsolute :: (Ord a, Num a) => a -> a -> a -> Bool
equalsWithinAbsolute expected actual tolerance =
    abs (expected - actual) <= tolerance

equalsWithinRelative :: (Ord a, Fractional a) => a -> a -> a -> Bool
equalsWithinRelative expected actual tolerance =
    ((expected == 0) && (actual == 0))
        || (abs (expected - actual) / abs expected <= tolerance)
