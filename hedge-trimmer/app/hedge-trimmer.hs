module Main
    ( main
    )
where

import           Lib                            ( run )
import           System.Environment             ( getArgs )


main :: IO ()
main = getArgs >>= run
