module Lib
    ( run
    )
where

import           Control.Arrow                  ( (&&&) )
import           Control.Monad                  ( filterM )
import           Control.Applicative            ( (<|>) )
import           Data.Char                      ( isAlphaNum
                                                , isDigit
                                                , isUpper
                                                , toLower
                                                )
import           Data.List                      ( intercalate
                                                , sortOn
                                                , stripPrefix
                                                )
import           Data.Maybe                     ( mapMaybe )
import           System.Directory               ( doesDirectoryExist
                                                , doesFileExist
                                                , getDirectoryContents
                                                )
import           System.Environment             ( getProgName )
import           System.Exit                    ( exitFailure )
import           System.FilePath                ( FilePath
                                                , (</>)
                                                , splitDirectories
                                                , splitFileName
                                                )
import           System.IO                      ( hPutStrLn
                                                , stderr
                                                )

data Test = Test {
  testFile :: FilePath
, testModule :: String
}

run :: [String] -> IO ()
run args = do
    name <- getProgName
    case args of
        [src, _, dst] -> do
            tests <- findTests src
            writeFile dst (mkTestModule src tests)
        _ -> do
            hPutStrLn stderr (usage name)
            exitFailure

findTests :: FilePath -> IO [Test]
findTests src = do
    let (dir, file) = splitFileName src
    mapMaybe (fileToTest dir) . filter (/= file) <$> getFilesRecursive dir

fileToTest :: FilePath -> FilePath -> Maybe Test
fileToTest dir file = case reverse $ splitDirectories file of
    x : xs -> case stripSuffix "Test.hs" x <|> stripSuffix "Test.lhs" x of
        Just name | isValidModuleName name && all isValidModuleName xs ->
            Just . Test (dir </> file) $ (intercalate "." . reverse) (name : xs)
        _ -> Nothing
    _ -> Nothing
  where
    stripSuffix :: Eq a => [a] -> [a] -> Maybe [a]
    stripSuffix suffix str =
        reverse <$> stripPrefix (reverse suffix) (reverse str)

-- See `Cabal.Distribution.ModuleName` (http://git.io/bj34)
isValidModuleName :: String -> Bool
isValidModuleName []       = False
isValidModuleName (c : cs) = isUpper c && all isValidModuleChar cs

isValidModuleChar :: Char -> Bool
isValidModuleChar c = isAlphaNum c || c == '_' || c == '\''

getFilesRecursive :: FilePath -> IO [FilePath]
getFilesRecursive baseDir = sortNaturally <$> go []
  where
    go :: FilePath -> IO [FilePath]
    go dir = do
        c <-
            map (dir </>)
            .   filter (`notElem` [".", ".."])
            <$> getDirectoryContents (baseDir </> dir)
        dirs  <- filterM (doesDirectoryExist . (baseDir </>)) c >>= mapM go
        files <- filterM (doesFileExist . (baseDir </>)) c
        return (files ++ concat dirs)

sortNaturally :: [String] -> [String]
sortNaturally = sortOn naturalSortKey

newtype NaturalSortKey = NaturalSortKey [Chunk]
  deriving (Eq, Ord)

data Chunk = Numeric Integer Int | Textual [(Char, Char)]
  deriving (Eq, Ord)

naturalSortKey :: String -> NaturalSortKey
naturalSortKey = NaturalSortKey . chunks
  where
    chunks [] = []
    chunks s@(c : _)
        | isDigit c = Numeric (read num) (length num) : chunks afterNum
        | otherwise = Textual (map (toLower &&& id) str) : chunks afterStr
      where
        (num, afterNum) = span isDigit s
        (str, afterStr) = break isDigit s

mkTestModule :: FilePath -> [Test] -> String
mkTestModule src tests =
    "module Main where\n"
        ++ importList tests
        ++ "import Hedge\n"
        ++ "main :: IO ()\n"
        ++ "main = runTest test\n"
        ++ "test :: IO (Test ())\n"
        ++ "test = testThat "
        ++ formatTests tests
        ++ "\n"

importList :: [Test] -> String
importList = concatMap toImport
  where
    toImport :: Test -> String
    toImport test = "import qualified " ++ testModule test ++ "Test\n"

formatTests :: [Test] -> String
formatTests []    = "[]"
formatTests tests = "[" ++ intercalate ", " (map formatTest tests) ++ "]"

formatTest :: Test -> String
formatTest (Test _ name) = name ++ "Test.test"

usage :: String -> String
usage prog = "\nUsage: " ++ prog ++ " SRC CUR DST\n"
