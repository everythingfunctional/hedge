module PassingCollectionTest
    ( test
    )
where

import           ExampleAsserts                 ( successMessage )
import           ExampleCollections             ( exampleCaseDescription1
                                                , exampleCaseDescription2
                                                , exampleCollectionDescription
                                                , examplePassingCollection
                                                , numAssertsInPassing
                                                , numCasesInPassing
                                                )
import           Hedge                          ( Test
                                                , and
                                                , assertEmpty
                                                , assertEquals
                                                , assertIncludes
                                                , assertThat
                                                , countAllAsserts
                                                , countAllCases
                                                , countFailingAsserts
                                                , countFailingCases
                                                , failureDescription
                                                , given
                                                , passed
                                                , run
                                                , then'
                                                , verboseDescription
                                                , whenWithInput
                                                )
import           Prelude                 hiding ( and )
import           Test.QuickCheck.Counterexamples
                                                ( chatty
                                                , stdArgs
                                                )

test :: IO (Test ())
test = do
    result <- run (stdArgs { chatty = False }) examplePassingCollection
    return $ given
        "a passing test collection"
        [ whenWithInput
              "it is run"
              result
              [ then' "it knows it passed"                 checkCollectionPasses
              , then' "it knows how many cases there were" checkNumCases
              , then' "it has no failing cases"            checkNumFailingCases
              , then'
                  "it's verbose description includes the given description"
                  checkVerboseTopDescription
              , then'
                  "it's verbose description includes the individual case descriptions"
                  checkVerboseCaseDescriptions
              , then'
                  "it's verbose description includes the assertion message"
                  checkVerboseDescriptionAssertion
              , then' "it's failure description is empty"
                      checkFailureDescriptionEmpty
              , then' "it knows how many asserts there were" checkNumAsserts
              , then' "it has no failing asserts" checkNumFailingAsserts
              ]
        ]

checkCollectionPasses result = assertThat (passed result)

checkNumCases result = assertEquals numCasesInPassing (countAllCases result)

checkNumFailingCases result =
    assertEquals (0 :: Integer) (countFailingCases result)

checkVerboseTopDescription result = assertIncludes
    exampleCollectionDescription
    (verboseDescription False result)

checkVerboseCaseDescriptions result =
    let description = verboseDescription False result
    in  assertIncludes exampleCaseDescription1 description
            `and` assertIncludes exampleCaseDescription2 description

checkVerboseDescriptionAssertion result =
    assertIncludes successMessage (verboseDescription False result)

checkFailureDescriptionEmpty result =
    assertEmpty $ failureDescription False result

checkNumAsserts result =
    assertEquals numAssertsInPassing (countAllAsserts result)

checkNumFailingAsserts result =
    assertEquals (0 :: Integer) (countFailingAsserts result)
