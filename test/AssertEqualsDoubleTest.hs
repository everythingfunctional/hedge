module AssertEqualsDoubleTest
    ( test
    )
where

import           Hedge                          ( Result
                                                , Test
                                                , assertEquals
                                                , assertNotWithMessage
                                                , assertThatWithMessage
                                                , describe
                                                , it
                                                , itAlways
                                                , passed'
                                                , verboseDescription'
                                                )

test :: IO (Test ())
test = return $ describe
    "assertEquals with Double"
    [ itAlways "passes with the same number" checkPassForSameNumber
    , it "fails with different numbers" checkFailForDifferentNumberes
    ]

checkPassForSameNumber :: Double -> Result
checkPassForSameNumber value =
    let result = assertEquals value value
    in  assertThatWithMessage (passed' result)
                              (concat (verboseDescription' False result))

checkFailForDifferentNumberes =
    let result = assertEquals 1.0 (2.0 :: Double)
    in  assertNotWithMessage (passed' result)
                             (concat (verboseDescription' False result))
