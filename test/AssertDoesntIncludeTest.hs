module AssertDoesntIncludeTest
    ( test
    )
where

import           Hedge                          ( Result
                                                , Test
                                                , assertDoesntInclude
                                                , assertNotWithMessage
                                                , assertThatWithMessage
                                                , describe
                                                , it
                                                , itAlways
                                                , passed'
                                                , verboseDescription'
                                                )

test :: IO (Test ())
test = return $ describe
    "assertDoesntInclude"
    [ it "passes for different strings" checkPassForDifferentStrings
    , itAlways "fails with the same string" checkFailForSameString
    ]

checkPassForDifferentStrings =
    let result = assertDoesntInclude "One String" "Other String"
    in  assertThatWithMessage (passed' result)
                              (concat (verboseDescription' False result))

checkFailForSameString :: String -> Result
checkFailForSameString string =
    let result = assertDoesntInclude string string
    in  assertNotWithMessage (passed' result)
                             (concat (verboseDescription' False result))
