module ExampleAsserts
    ( exampleMultipleAsserts
    , exampleMultipleAssertsWithFailure
    , failureMessage
    , numFailingAssertsInFailing
    , numPassingAssertsInFailing
    , numAssertsInFailing
    , numAssertsInPassing
    , successMessage
    )
where

import           Hedge                          ( and
                                                , fail'
                                                , succeed
                                                )
import           Prelude                 hiding ( and )

failureMessage = "Failure Message"
numFailingAssertsInFailing = 1
numPassingAssertsInFailing = 1
numAssertsInFailing = numFailingAssertsInFailing + numPassingAssertsInFailing
numAssertsInPassing = 2
successMessage = "Success Message"

exampleMultipleAsserts = succeed successMessage `and` succeed successMessage
exampleMultipleAssertsWithFailure =
    succeed successMessage `and` fail' failureMessage
