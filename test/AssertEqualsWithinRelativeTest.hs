module AssertEqualsWithinRelativeTest
    ( test
    )
where

import           Hedge                          ( Result
                                                , Test
                                                , assertEqualsWithinRelative
                                                , assertNotWithMessage
                                                , assertThatWithMessage
                                                , describe
                                                , itAlways
                                                , passed'
                                                , verboseDescription'
                                                )
import           Numeric.Limits                 ( minValue )

test :: IO (Test ())
test = return $ describe
    "assertEqualsWithinRelative"
    [ itAlways "passes with the same number even with very small tolerance"
               checkPassForSameNumber
    , itAlways "fails with sufficiently different numbers"
               checkFailForDifferentNumbers
    , itAlways "passes with sufficiently close numbers" checkPassForCloseNumbers
    ]

checkPassForSameNumber :: Double -> Result
checkPassForSameNumber value =
    let result = assertEqualsWithinRelative value value minValue
    in  assertThatWithMessage (passed' result)
                              (concat (verboseDescription' False result))

checkFailForDifferentNumbers :: Double -> Result
checkFailForDifferentNumbers value =
    let result = assertEqualsWithinRelative
            value
            (if value == 0 then 0.1 else value * 1.11)
            0.1
    in  assertNotWithMessage (passed' result)
                             (concat (verboseDescription' False result))

checkPassForCloseNumbers :: Double -> Result
checkPassForCloseNumbers value =
    let result = assertEqualsWithinRelative value (value * 1.09) 0.1
    in  assertThatWithMessage (passed' result)
                              (concat (verboseDescription' False result))
