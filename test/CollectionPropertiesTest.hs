module CollectionPropertiesTest
    ( test
    )
where

import           ExampleCollections             ( exampleCaseDescription1
                                                , exampleCaseDescription2
                                                , exampleCollectionDescription
                                                , examplePassingCollection
                                                , numCasesInPassing
                                                )
import           Hedge                          ( Test
                                                , and
                                                , assertEquals
                                                , assertIncludes
                                                , count
                                                , description
                                                , givenInput
                                                , then'
                                                )
import           Prelude                 hiding ( and )

test :: IO (Test ())
test = return $ givenInput
    "a test collection"
    examplePassingCollection
    [ then' "it can tell how many tests it has" checkNumCases
    , then' "it includes the given description" checkCollectionTopDescription
    , then' "it includes the individual test descriptions"
            checkCollectionDescriptions
    ]

checkNumCases theCollection =
    assertEquals numCasesInPassing $ count theCollection

checkCollectionTopDescription theCollection =
    assertIncludes exampleCollectionDescription $ description theCollection

checkCollectionDescriptions theCollection =
    assertIncludes exampleCaseDescription1 (description theCollection)
        `and` assertIncludes exampleCaseDescription2 (description theCollection)
