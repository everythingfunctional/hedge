module FailingCollectionTest
    ( test
    )
where

import           ExampleAsserts                 ( successMessage )
import           ExampleCollections             ( exampleCaseDescription1
                                                , exampleCaseDescription2
                                                , exampleFailingCaseDescription
                                                , exampleCollectionDescription
                                                , exampleFailingCollection
                                                , failureMessage
                                                , numAssertsInFailing
                                                , numCasesInFailing
                                                , numFailingAsserts
                                                , numFailingCases
                                                )
import           Hedge                          ( Test
                                                , and
                                                , assertDoesntInclude
                                                , assertEquals
                                                , assertIncludes
                                                , assertNot
                                                , countAllAsserts
                                                , countAllCases
                                                , countFailingAsserts
                                                , countFailingCases
                                                , failureDescription
                                                , given
                                                , passed
                                                , run
                                                , then'
                                                , verboseDescription
                                                , whenWithInput
                                                )
import           Prelude                 hiding ( and )
import           Test.QuickCheck.Counterexamples
                                                ( chatty
                                                , stdArgs
                                                )

test :: IO (Test ())
test = do
    result <- run (stdArgs { chatty = False }) exampleFailingCollection
    return $ given
        "a failing test collection"
        [ whenWithInput
              "it is run"
              result
              [ then' "it knows it failed"                 checkCollectionFails
              , then' "it knows how many cases there were" checkNumCases
              , then' "it knows how many cases failed"     checkNumFailingCases
              , then'
                  "it's verbose description includes the given description"
                  checkVerboseTopDescription
              , then'
                  "it's verbose description includes the individual case descriptions"
                  checkVerboseCaseDescriptions
              , then'
                  "it's verbose description includes the failure message"
                  checkVerboseForFailureMessage
              , then'
                  "it's verbose description includes the success message"
                  checkVerboseForSuccessMessage
              , then'
                  "it's failure description includes the given description"
                  checkFailureForTopDescription
              , then'
                  "it's failure description includes the failing case description"
                  checkFailureCaseDescription
              , then'
                  "it's failure description does not include the passing case descriptions"
                  checkFailureNoPassingDescriptions
              , then'
                  "it's failure description includes the failure message"
                  checkFailureForMessage
              , then'
                  "it's failure description does not include the success message"
                  checkFailureNoSuccessMessage
              , then' "it knows how many asserts there were" checkNumAsserts
              , then' "it knows how many asserts failed" checkNumFailingAsserts
              ]
        ]

checkCollectionFails result = assertNot (passed result)

checkNumCases result = assertEquals numCasesInFailing (countAllCases result)

checkNumFailingCases result =
    assertEquals numFailingCases (countFailingCases result)

checkVerboseTopDescription result = assertIncludes
    exampleCollectionDescription
    (verboseDescription False result)

checkVerboseCaseDescriptions result =
    let description = verboseDescription False result
    in  assertIncludes exampleCaseDescription1 description
            `and` assertIncludes exampleCaseDescription2       description
            `and` assertIncludes exampleFailingCaseDescription description

checkVerboseForFailureMessage result =
    assertIncludes failureMessage (verboseDescription False result)

checkVerboseForSuccessMessage result =
    assertIncludes successMessage (verboseDescription False result)

checkFailureForTopDescription result = assertIncludes
    exampleCollectionDescription
    (failureDescription False result)

checkFailureCaseDescription result = assertIncludes
    exampleFailingCaseDescription
    (failureDescription False result)

checkFailureNoPassingDescriptions result =
    let description = failureDescription False result
    in  assertDoesntInclude exampleCaseDescription1 description
            `and` assertDoesntInclude exampleCaseDescription2 description

checkFailureForMessage result =
    assertIncludes failureMessage (failureDescription False result)

checkFailureNoSuccessMessage result =
    assertDoesntInclude successMessage (failureDescription False result)

checkNumAsserts result =
    assertEquals numAssertsInFailing (countAllAsserts result)

checkNumFailingAsserts result =
    assertEquals numFailingAsserts (countFailingAsserts result)
