module AssertEqualsWithinAbsoluteTest
    ( test
    )
where

import           Hedge                          ( Result
                                                , Test
                                                , assertEqualsWithinAbsolute
                                                , assertNotWithMessage
                                                , assertThatWithMessage
                                                , describe
                                                , itAlways
                                                , passed'
                                                , verboseDescription'
                                                )
import           Numeric.Limits                 ( minValue )

test :: IO (Test ())
test = return $ describe
    "assertEqualsWithinAbsolute"
    [ itAlways "passes with the same number even with very small tolerance"
               checkPassForSameNumber
    , itAlways "fails with sufficiently different numbers"
               checkFailForDifferentNumbers
    , itAlways "passes with sufficiently close numbers" checkPassForCloseNumbers
    ]

checkPassForSameNumber :: Double -> Result
checkPassForSameNumber value =
    let result = assertEqualsWithinAbsolute value value minValue
    in  assertThatWithMessage (passed' result)
                              (concat (verboseDescription' False result))

checkFailForDifferentNumbers :: Double -> Result
checkFailForDifferentNumbers value =
    let result = assertEqualsWithinAbsolute value (value + 0.2) 0.1
    in  assertNotWithMessage (passed' result)
                             (concat (verboseDescription' False result))

checkPassForCloseNumbers :: Double -> Result
checkPassForCloseNumbers value =
    let result = assertEqualsWithinAbsolute value (value + 0.05) 0.1
    in  assertThatWithMessage (passed' result)
                              (concat (verboseDescription' False result))
