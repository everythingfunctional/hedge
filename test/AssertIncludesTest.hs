module AssertIncludesTest
    ( test
    )
where

import           Hedge                          ( Result
                                                , Test
                                                , assertIncludes
                                                , assertNotWithMessage
                                                , assertThatWithMessage
                                                , describe
                                                , it
                                                , itAlways
                                                , passed'
                                                , verboseDescription'
                                                )

test :: IO (Test ())
test = return $ describe
    "assertIncludes"
    [ itAlways "passes with the same strings" checkPassForSameStrings
    , it "fails when the string isn't included" checkFailForDifferentStrings
    ]

checkPassForSameStrings :: String -> Result
checkPassForSameStrings string =
    let result = assertIncludes string string
    in  assertThatWithMessage (passed' result)
                              (concat (verboseDescription' False result))

checkFailForDifferentStrings =
    let result = assertIncludes "One String" "Other String"
    in  assertNotWithMessage (passed' result)
                             (concat (verboseDescription' False result))
