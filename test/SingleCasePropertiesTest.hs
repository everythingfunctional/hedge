module SingleCasePropertiesTest
    ( test
    )
where

import           ExampleCases                   ( exampleDescription
                                                , examplePassingTestCase
                                                )
import           Hedge                          ( Test
                                                , assertEquals
                                                , assertIncludes
                                                , count
                                                , description
                                                , givenInput
                                                , then'
                                                )

test :: IO (Test ())
test = return $ givenInput
    "a test case"
    examplePassingTestCase
    [ then' "it includes the given description" checkCaseDescription
    , then' "it only has one test case"         checkNumCases
    ]

checkCaseDescription theCase =
    assertIncludes exampleDescription (description theCase)

checkNumCases theCase = assertEquals (1 :: Int) (count theCase)
