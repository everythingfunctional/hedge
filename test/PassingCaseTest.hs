module PassingCaseTest
    ( test
    )
where

import           ExampleAsserts                 ( numAssertsInPassing
                                                , successMessage
                                                )
import           ExampleCases                   ( exampleDescription
                                                , examplePassingTestCase
                                                )
import           Hedge                          ( Test
                                                , assertEmpty
                                                , assertEquals
                                                , assertIncludes
                                                , assertThat
                                                , countAllAsserts
                                                , countAllCases
                                                , countFailingAsserts
                                                , countFailingCases
                                                , failureDescription
                                                , given
                                                , passed
                                                , run
                                                , then'
                                                , verboseDescription
                                                , whenWithInput
                                                )
import           Test.QuickCheck.Counterexamples
                                                ( chatty
                                                , stdArgs
                                                )

test :: IO (Test ())
test = do
    result <- run (stdArgs { chatty = False }) examplePassingTestCase
    return $ given
        "a passing test case"
        [ whenWithInput
              "it is run"
              result
              [ then' "it knows it passed"     checkCasePasses
              , then' "it has 1 test case"     checkNumCases
              , then' "it has no failing case" checkNumFailingCases
              , then'
                  "it's verbose description still includes the given description"
                  checkVerboseDescription
              , then'
                  "it's verbose description includes the assertion message"
                  checkVerboseDescriptionAssertion
              , then' "it's failure description is empty"
                      checkFailureDescriptionEmpty
              , then' "it knows how many asserts there were" checkNumAsserts
              , then' "it has no failing asserts" checkNumFailingAsserts
              ]
        ]

checkCasePasses result = assertThat (passed result)

checkNumCases result = assertEquals (1 :: Int) $ countAllCases result

checkNumFailingCases result =
    assertEquals (0 :: Int) $ countFailingCases result

checkVerboseDescription result =
    assertIncludes exampleDescription $ verboseDescription False result

checkVerboseDescriptionAssertion result =
    assertIncludes successMessage $ verboseDescription False result

checkFailureDescriptionEmpty result =
    assertEmpty $ failureDescription False result

checkNumAsserts result =
    assertEquals numAssertsInPassing $ countAllAsserts result

checkNumFailingAsserts result =
    assertEquals (0 :: Int) $ countFailingAsserts result
