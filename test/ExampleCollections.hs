module ExampleCollections
    ( exampleCaseDescription1
    , exampleCaseDescription2
    , exampleCollectionDescription
    , exampleFailingCaseDescription
    , exampleFailingCollection
    , examplePassingCollection
    , failureMessage
    , middleCollectionDescription
    , notInDescriptions
    , numAssertsInFailing
    , numAssertsInPassing
    , numCasesInFailing
    , numCasesInPassing
    , numFailingAsserts
    , numFailingCases
    )
where

import qualified ExampleAsserts                as EA
import           Hedge                          ( describe
                                                , fail'
                                                , it
                                                )

exampleCaseDescription1 = "Example Case Description 1"
exampleCaseDescription2 = "Example Case Description 2"
exampleCollectionDescription = "Example Collection Description"
exampleFailingCaseDescription = "Example Failing Case Description"
failureMessage = "Failure Message"
middleCollectionDescription = "Middle Collection Description"
notInDescriptions = "NOT IN DESCRIPTION"
numAssertsInPassing = EA.numAssertsInPassing * 3
numAssertsInFailing = EA.numAssertsInPassing * 2 + 1
numCasesInPassing = 3
numCasesInFailing = 3
numFailingAsserts = 1
numFailingCases = 1

exampleTestCase1 = it exampleCaseDescription1 EA.exampleMultipleAsserts
exampleTestCase2 = it exampleCaseDescription2 EA.exampleMultipleAsserts
exampleFail = fail' failureMessage
exampleFailingTestCase = it exampleFailingCaseDescription exampleFail
exampleFailingCollection = describe
    exampleCollectionDescription
    [exampleTestCase1, exampleTestCase2, exampleFailingTestCase]
middleCollection =
    describe middleCollectionDescription [exampleTestCase1, exampleTestCase2]
examplePassingCollection =
    describe exampleCollectionDescription [middleCollection, exampleTestCase2]
