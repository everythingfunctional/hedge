module FailingCaseTest
    ( test
    )
where

import           ExampleAsserts                 ( failureMessage
                                                , numAssertsInFailing
                                                , numFailingAssertsInFailing
                                                , successMessage
                                                )
import           ExampleCases                   ( exampleFailingTestCase
                                                , exampleDescription
                                                )
import           Hedge                          ( Test
                                                , assertDoesntInclude
                                                , assertEquals
                                                , assertIncludes
                                                , assertNot
                                                , countAllAsserts
                                                , countAllCases
                                                , countFailingAsserts
                                                , countFailingCases
                                                , failureDescription
                                                , given
                                                , passed
                                                , run
                                                , then'
                                                , verboseDescription
                                                , whenWithInput
                                                )
import           Test.QuickCheck.Counterexamples
                                                ( chatty
                                                , stdArgs
                                                )

test :: IO (Test ())
test = do
    result <- run (stdArgs { chatty = False }) exampleFailingTestCase
    return $ given
        "a failing test case"
        [ whenWithInput
              "it is run"
              result
              [ then' "it knows it failed"    checkCaseFails
              , then' "it has 1 test case"    checkNumCases
              , then' "it has 1 failing case" checkNumFailingCases
              , then'
                  "it's verbose description includes the given description"
                  checkVerboseForGivenDescription
              , then'
                  "it's verbose description includes the success message"
                  checkVerboseForSuccessMessage
              , then'
                  "it's verbose description includes the failure message"
                  checkVerboseForFailureMessage
              , then'
                  "it's failure description includes the given description"
                  checkFailureForGivenDescription
              , then'
                  "it's failure description includes the failure message"
                  checkFailureForFailureMessage
              , then'
                  "it's failure description doesn't include the success message"
                  checkFailureNoSuccessMessage
              , then' "it knows how many asserts there were" checkNumAsserts
              , then' "it knows how many asserts failed" checkNumFailingAsserts
              ]
        ]

checkCaseFails result = assertNot (passed result)

checkNumCases result = assertEquals (1 :: Integer) (countAllCases result)

checkNumFailingCases result =
    assertEquals (1 :: Integer) (countFailingCases result)

checkVerboseForGivenDescription result =
    assertIncludes exampleDescription (verboseDescription False result)

checkVerboseForSuccessMessage result =
    assertIncludes successMessage (verboseDescription False result)

checkVerboseForFailureMessage result =
    assertIncludes failureMessage (verboseDescription False result)

checkFailureForGivenDescription result =
    assertIncludes exampleDescription (failureDescription False result)

checkFailureForFailureMessage result =
    assertIncludes failureMessage (failureDescription False result)

checkFailureNoSuccessMessage result =
    assertDoesntInclude successMessage (failureDescription False result)

checkNumAsserts result =
    assertEquals numAssertsInFailing (countAllAsserts result)

checkNumFailingAsserts result =
    assertEquals numFailingAssertsInFailing (countFailingAsserts result)
