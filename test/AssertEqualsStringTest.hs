module AssertEqualsStringTest
    ( test
    )
where

import           Hedge                          ( Result
                                                , Test
                                                , assertEquals
                                                , assertNotWithMessage
                                                , assertThatWithMessage
                                                , describe
                                                , it
                                                , itAlways
                                                , passed'
                                                , verboseDescription'
                                                )

test :: IO (Test ())
test = return $ describe
    "assertEquals with strings"
    [ itAlways "passes with the same string" checkPassForSameString
    , it "fails with different strings" checkFailForDifferentStrings
    ]

checkPassForSameString :: String -> Result
checkPassForSameString string =
    let result = assertEquals string string
    in  assertThatWithMessage (passed' result)
                              (concat (verboseDescription' False result))

checkFailForDifferentStrings =
    let result = assertEquals "One String" "Another String"
    in  assertNotWithMessage (passed' result)
                             (concat (verboseDescription' False result))
