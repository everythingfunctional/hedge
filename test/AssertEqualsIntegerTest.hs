module AssertEqualsIntegerTest
    ( test
    )
where

import           Hedge                          ( Result
                                                , Test
                                                , assertEquals
                                                , assertNotWithMessage
                                                , assertThatWithMessage
                                                , describe
                                                , it
                                                , itAlways
                                                , passed'
                                                , verboseDescription'
                                                )

test :: IO (Test ())
test = return $ describe
    "assertEquals with integers"
    [ itAlways "passes with the same integer" checkPassForSameInteger
    , it "fails with different integers" checkFailForDifferentInteger
    ]

checkPassForSameInteger :: Integer -> Result
checkPassForSameInteger input =
    let example_result = assertEquals input input
    in  assertThatWithMessage
            (passed' example_result)
            (concat (verboseDescription' False example_result))

checkFailForDifferentInteger :: Result
checkFailForDifferentInteger =
    let example_result = assertEquals 1 (2 :: Integer)
    in  assertNotWithMessage
            (passed' example_result)
            (concat (verboseDescription' False example_result))
