module ExampleCases
    ( exampleDescription
    , exampleFailingTestCase
    , examplePassingTestCase
    , notInDescription
    )
where

import           ExampleAsserts                 ( exampleMultipleAsserts
                                                , exampleMultipleAssertsWithFailure
                                                )
import           Hedge                          ( it )

exampleDescription = "Example description"
notInDescription = "NOT"

examplePassingTestCase = it exampleDescription exampleMultipleAsserts
exampleFailingTestCase =
    it exampleDescription exampleMultipleAssertsWithFailure
