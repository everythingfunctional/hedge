module AssertEmptyTest
    ( test
    )
where

import           Hedge                          ( Test
                                                , assertEmpty
                                                , assertNotWithMessage
                                                , assertThatWithMessage
                                                , describe
                                                , it
                                                , passed'
                                                , verboseDescription'
                                                )

test :: IO (Test ())
test = return $ describe
    "assertEmpty"
    [ it "passes with an empty string"   checkPassForEmptyString
    , it "fails with a non-empty string" checkFailsForNonEmptyString
    ]

checkPassForEmptyString =
    let result = assertEmpty ""
    in  assertThatWithMessage (passed' result)
                              (concat (verboseDescription' False result))

checkFailsForNonEmptyString =
    let result = assertEmpty "Not Empty"
    in  assertNotWithMessage (passed' result)
                             (concat (verboseDescription' False result))
